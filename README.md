# JavaScript project 1 Noroff autumn 2020

## A simple bank/ computer webstore

### Instructions
- When loading index.html, enter your name.
- To earn money, click the 'Work' button.
- To bank your pay, click the 'Bank' button.
- To loan money, click the 'loan' button and enter desired amount.
- Use the dropdown menu to choose a computer to buy.
- Click 'Buy Now!' to buy the computer.

### Files
- index.html
- komputerstore.js
- style.css

#### Notes
- I would have liked to make the top cards' height syncrinously dynamic.
- Would have liked to put buttons at bottom of cards.