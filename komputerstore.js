// get DOMS
const domUserName = document.getElementById('username');
const domBalance = document.getElementById('balance');
const domPay = document.getElementById('pay');
const domComputers = document.getElementById('computers');
const domComputerName = document.getElementById('computer-name');
const domComputerDescription = document.getElementById('computer-description');
const domComputerPrice = document.getElementById('computer-price');
const domComputerImage = document.getElementById('computer-image');
const domFeatures = document.getElementById('computer-features');

// Global variables
let currentBalance = 0;
let totalPay = 0;
let hasLoan = false;
let currentComputerPrice = 0;
let computers = [
    {price: 9999,
    name: 'Crapple Scrapbook',
    features: 'Looks really nice. Is lightweight and trendy. Makes you a better person',
    description: 'Do not get fooled by the looks. It looks like a scrapbook, but it is a computer. '
    + 'It will make you poor, but boost your self-confidence.',
    img: 'https://www.norli.no/media/catalog/product/cache/dce547051845b8b9885dfcbb94b13b67/5/0/5055134095847.jpg'},
    {price: 5000,
    name: 'Dull Dimunchion',
    features: '64 GB RAM. Octacore 3.4 GHz quantum processor. 16 GB video card. 1TB SSD',
    description: 'This computer is a real powerhouse. If you are a serious number cruncher, '
    + 'this will do the job.',
    img: 'https://www.reboot-it.com.au/assets/full/111033-B.gif?20200526130015://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Dell_Dimension_C521.jpg/330px-Dell_Dimension_C521.jpg'},
    {price: 500,
    name: 'FrogBook',
    features: 'Can say \'ribbit\'. Has nice colors. Can jump',
    description: 'This is our best budget buy. ',
    img: 'https://thumbs.dreamstime.com/b/computer-frog-5971842.jpg'},
    {price: 4000,
    name: 'Axxus Roxx',
    features: 'Portable and powerful. WeeMorse DDR 1337 graphics card. Full HD CRT screen ',
    description: 'If you like gaming, but still want a laptop, consider this computer with'
    + ' it\'s extra large battery.',
    img: 'https://www.notebookcheck.net/uploads/tx_nbc2/ROG_Strix_SCARIII_G531_04.jpg'}
]

// Get name of user
let userName = prompt("Welcome to the Ribbitstore! What is your name?");
domUserName.innerText = userName;

const doSomeWork = () => {
    // Increase pay and render
    totalPay += 100;
    domPay.innerText = totalPay + ' kr.';
}
const bankMoney = () => {
    // Increase balance and render
    currentBalance += totalPay;
    domBalance.innerText = currentBalance + ' kr.';
    // Reset Pay
    totalPay = 0;
    domPay.innerText = totalPay + ' kr.';
}
function getLoan(){
    // Click triggers prompt for user to input desired amount.
    let loan = parseInt(prompt("How much do you want to loan?\nMax: "+ 2*currentBalance));
    if(loan > 2*currentBalance){
        alert("You cannot loan more than twice your balance");
        return;
    }
    if(!hasLoan){
        currentBalance += loan;
        domBalance.innerText = currentBalance;
        hasLoan = true;
    }
}


// Show description before selection from dropdown
window.onload = changeComputer(domComputers);

// Render when selecting computer
function changeComputer(select){
    // Show computer features
    domFeatures.innerHTML = '';
    const currentFeatures = computers[select.value].features;
    const array = currentFeatures.split('. ');
    array.forEach((item)=>{
        const domItem = document.createElement('li');
        domItem.innerText = item;
        domFeatures.appendChild(domItem);
    });

    // Display computer name in bottom card
    domComputerName.innerText = computers[select.value].name;
    // Get illustration image
    domComputerImage.innerHTML = "";
    let img = document.createElement("img");
    img.src = computers[select.value].img;
    var src = domComputerImage;
    src.appendChild(img);
    // Display computer description
    domComputerDescription.innerText = computers[select.value].description;
    // Set currentComputerPrice
    currentComputerPrice = computers[select.value].price;
    // Display computer price
    domComputerPrice.innerHTML = currentComputerPrice + ' kr.';
}

// Buy a computer
function buyComputer(){
    if(currentBalance < currentComputerPrice){
        alert("You cannot afford this computer.");
        return;
    }
    currentBalance -= currentComputerPrice;
    // Update balance (oops, boilerplate? Render function?)
    domBalance.innerText = currentBalance;
    alert("Congratulations! You now have a new computer.");
}